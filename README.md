USB serial role
===============

Setup USB serial connections for remote consoles etc.

Requirements
------------

This role assumes the following:

  - User called `ansible` is present on the system
  - User `ansible` can use sudo without password
  - User `ansible` uses `/bin/sh` compatible shell

Role Variables
--------------

Variables that need to be present:

- `usb_serial`: List of USB serial ports to configure

Dependencies
------------

None

Example Playbook
----------------


```yaml
# inventory.yml
---
all:
  hosts:
    server1:
      usb_serial:
        - name: "core-switch"
          vendor: "2342"
          product: "1337"
          serial: "ABC123X"
```

```yaml
# playbook.yml
---
- hosts: all
  roles:
    - usb_serial
```

License
-------

MIT

Author Information
------------------

  - Ricardo (XenGi) Band <email@ricardo.band>

